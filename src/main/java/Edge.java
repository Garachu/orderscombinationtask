
/**
 * Created by Meg on 7/22/18.
 */
public class Edge {

    private Location target;
    private double weight;

    public Edge(Location target, double weight) {
        this.target = target;
        this.weight = weight;
    }

    public Location getTarget(){
        return this.target;
    }
    public double getWeight(){
        return this.weight;
    }
}