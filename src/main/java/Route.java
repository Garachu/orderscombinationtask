import java.util.List;

/**
 * Created by Meg on 7/22/18.
 */

/**
 * A route represents a point location and distance to that point.
 */
public class Route {

    private Double distance;

    private List<Location> pointLocation;

    Route(Double distance) {
        this.distance = distance;
    }

    public void setPointLocation(List<Location> pointLocation) {
        this.pointLocation = pointLocation;
    }

    public Double getDistance() {
        return this.distance;
    }

    public List<Location> getPointLocation() {
        return this.pointLocation;
    }
}