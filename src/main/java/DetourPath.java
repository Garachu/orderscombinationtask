
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Meg on 7/22/18.
 */

public class DetourPath {

    // Find Optimal path
    DetourPath(boolean optimalPath) {

        // Detour #1: routes
        Route A_C;
        Route C_D;
        Route D_B;

        // Detour #2: routes
        Route C_A;
        Route A_B;
        Route B_D;

        // Instantiate Destination Points
        HashMap<Character, Location> pointHash = createPoints();

        // Set the edges and weight to create a graph
        generateEdges(pointHash);

        // calculate sub routes from A to C, and A to B
        ToolKit.computePaths(pointHash.get('A'));

        // Assign partial route weights from Node A to be compared later
        A_C = new Route(pointHash.get('C').getMinimumDistance());
        A_B = new Route(pointHash.get('B').getMinimumDistance());

        A_C.setPointLocation(ToolKit.getShortestPath(pointHash.get('C')));
        A_B.setPointLocation(ToolKit.getShortestPath(pointHash.get('B')));

        // Reset all distances to Infinity
        removeAllDistances(pointHash);

        // calculate sub routes from C to D, and C to D
        ToolKit.computePaths(pointHash.get('C'));

        // Assign route weights from Node C to be compared later
        C_D = new Route(pointHash.get('D').getMinimumDistance());
        C_A = new Route(pointHash.get('A').getMinimumDistance());

        C_D.setPointLocation(ToolKit.getShortestPath(pointHash.get('D')));
        C_A.setPointLocation(ToolKit.getShortestPath(pointHash.get('A')));

        // Reset all distances to Infinity
        removeAllDistances(pointHash);

        // Calculate sub routes from D to B
        ToolKit.computePaths(pointHash.get('D'));

        // Assign partial route weights from Node D to be compared later
        D_B = new Route(pointHash.get('B').getMinimumDistance());

        D_B.setPointLocation(ToolKit.getShortestPath(pointHash.get('B')));

        removeAllDistances(pointHash);

        // Run ToolKit from B to calculate sub routes from B to D
        ToolKit.computePaths(pointHash.get('B'));

        // Assign partial route weights from Node B to be compared later
        B_D = new Route(pointHash.get('D').getMinimumDistance());

        // Add the optimal node path
        B_D.setPointLocation(ToolKit.getShortestPath(pointHash.get('D')));

        // Sum up the detour paths to be compared
        double detourForA = ToolKit.computeFullDistance(A_C, C_D, D_B);
        double detourForB = ToolKit.computeFullDistance(C_A, A_B, B_D);

        if (optimalPath && (detourForA < detourForB)) {
            System.out.println("Start from A!");

            // Join partial paths from A to C, C to D, and D to B.
            List<Location> route = new ArrayList<Location>(A_C.getPointLocation());
            List<Location> C_DLessEnd = C_D.getPointLocation();

            // removal of common node between C and D
            C_DLessEnd.remove(0);
            route.addAll(C_DLessEnd);
            List<Location> D_BLessEnd = D_B.getPointLocation();

            // removal of common node between D and B
            D_BLessEnd.remove(0);
            route.addAll(D_BLessEnd);

            System.out.println(route);
            System.out.println("Total Distance: " + detourForA);
        } else {
            System.out.println("Start from B!");

            // Join partial paths from C to A, A to B, and B to D.
            List<Location> route = new ArrayList<Location>(C_A.getPointLocation());
            List<Location> A_BLessEnd = A_B.getPointLocation();

            // removal of common node between A and B
            A_BLessEnd.remove(0);
            route.addAll(A_BLessEnd);
            List<Location> B_DLessEnd = B_D.getPointLocation();

            // removal of common node between B and D
            B_DLessEnd.remove(0);
            route.addAll(B_DLessEnd);

            System.out.println(route);
            System.out.println("Total Distance: " + detourForB);
        }
    }

    private void generateEdges(HashMap<Character, Location> pointHash) {
        pointHash.get('A').setAdjacencies(new Edge[]{new Edge(pointHash.get('E'), ToolKit.computeEdges(pointHash.get('A'), pointHash.get('E'))), new Edge(pointHash.get('F'), ToolKit.computeEdges(pointHash.get('A'), pointHash.get('F')))});
        pointHash.get('B').setAdjacencies(new Edge[]{new Edge(pointHash.get('P'), ToolKit.computeEdges(pointHash.get('P'), pointHash.get('B'))), new Edge(pointHash.get('N'), ToolKit.computeEdges(pointHash.get('N'), pointHash.get('B'))), new Edge(pointHash.get('K'), ToolKit.computeEdges(pointHash.get('K'), pointHash.get('B'))), new Edge(pointHash.get('J'), ToolKit.computeEdges(pointHash.get('J'), pointHash.get('B')))});
        pointHash.get('C').setAdjacencies(new Edge[]{new Edge(pointHash.get('H'), ToolKit.computeEdges(pointHash.get('C'), pointHash.get('H'))), new Edge(pointHash.get('E'), ToolKit.computeEdges(pointHash.get('C'), pointHash.get('E')))});
        pointHash.get('D').setAdjacencies(new Edge[]{new Edge(pointHash.get('M'), ToolKit.computeEdges(pointHash.get('M'), pointHash.get('D'))), new Edge(pointHash.get('L'), ToolKit.computeEdges(pointHash.get('L'), pointHash.get('D'))), new Edge(pointHash.get('N'), ToolKit.computeEdges(pointHash.get('N'), pointHash.get('D')))});
        pointHash.get('E').setAdjacencies(new Edge[]{new Edge(pointHash.get('A'), ToolKit.computeEdges(pointHash.get('E'), pointHash.get('A'))), new Edge(pointHash.get('F'), ToolKit.computeEdges(pointHash.get('E'), pointHash.get('F'))), new Edge(pointHash.get('C'), ToolKit.computeEdges(pointHash.get('C'), pointHash.get('E'))), new Edge(pointHash.get('G'), ToolKit.computeEdges(pointHash.get('G'), pointHash.get('E')))});
        pointHash.get('F').setAdjacencies(new Edge[]{new Edge(pointHash.get('A'), ToolKit.computeEdges(pointHash.get('A'), pointHash.get('F'))), new Edge(pointHash.get('E'), ToolKit.computeEdges(pointHash.get('F'), pointHash.get('E'))), new Edge(pointHash.get('G'), ToolKit.computeEdges(pointHash.get('G'), pointHash.get('F'))), new Edge(pointHash.get('M'), ToolKit.computeEdges(pointHash.get('M'), pointHash.get('F')))});
        pointHash.get('G').setAdjacencies(new Edge[]{new Edge(pointHash.get('E'), ToolKit.computeEdges(pointHash.get('E'), pointHash.get('G'))), new Edge(pointHash.get('F'), ToolKit.computeEdges(pointHash.get('F'), pointHash.get('G'))), new Edge(pointHash.get('I'), ToolKit.computeEdges(pointHash.get('I'), pointHash.get('G'))), new Edge(pointHash.get('L'), ToolKit.computeEdges(pointHash.get('L'), pointHash.get('G'))), new Edge(pointHash.get('K'), ToolKit.computeEdges(pointHash.get('K'), pointHash.get('G')))});
        pointHash.get('H').setAdjacencies(new Edge[]{new Edge(pointHash.get('C'), ToolKit.computeEdges(pointHash.get('C'), pointHash.get('H'))), new Edge(pointHash.get('I'), ToolKit.computeEdges(pointHash.get('I'), pointHash.get('H')))});
        pointHash.get('I').setAdjacencies(new Edge[]{new Edge(pointHash.get('H'), ToolKit.computeEdges(pointHash.get('H'), pointHash.get('I'))), new Edge(pointHash.get('G'), ToolKit.computeEdges(pointHash.get('G'), pointHash.get('I'))), new Edge(pointHash.get('J'), ToolKit.computeEdges(pointHash.get('J'), pointHash.get('I')))});
        pointHash.get('J').setAdjacencies(new Edge[]{new Edge(pointHash.get('I'), ToolKit.computeEdges(pointHash.get('I'), pointHash.get('J'))), new Edge(pointHash.get('K'), ToolKit.computeEdges(pointHash.get('K'), pointHash.get('J'))), new Edge(pointHash.get('B'), ToolKit.computeEdges(pointHash.get('B'), pointHash.get('J')))});
        pointHash.get('K').setAdjacencies(new Edge[]{new Edge(pointHash.get('G'), ToolKit.computeEdges(pointHash.get('K'), pointHash.get('G'))), new Edge(pointHash.get('L'), ToolKit.computeEdges(pointHash.get('K'), pointHash.get('L'))), new Edge(pointHash.get('B'), ToolKit.computeEdges(pointHash.get('K'), pointHash.get('B'))), new Edge(pointHash.get('J'), ToolKit.computeEdges(pointHash.get('K'), pointHash.get('J')))});
        pointHash.get('L').setAdjacencies(new Edge[]{new Edge(pointHash.get('D'), ToolKit.computeEdges(pointHash.get('D'), pointHash.get('L'))), new Edge(pointHash.get('K'), ToolKit.computeEdges(pointHash.get('K'), pointHash.get('L'))), new Edge(pointHash.get('N'), ToolKit.computeEdges(pointHash.get('N'), pointHash.get('L'))), new Edge(pointHash.get('G'), ToolKit.computeEdges(pointHash.get('G'), pointHash.get('L')))});
        pointHash.get('M').setAdjacencies(new Edge[]{new Edge(pointHash.get('F'), ToolKit.computeEdges(pointHash.get('F'), pointHash.get('M'))), new Edge(pointHash.get('D'), ToolKit.computeEdges(pointHash.get('D'), pointHash.get('M'))), new Edge(pointHash.get('O'), ToolKit.computeEdges(pointHash.get('O'), pointHash.get('M')))});
        pointHash.get('N').setAdjacencies(new Edge[]{new Edge(pointHash.get('L'), ToolKit.computeEdges(pointHash.get('L'), pointHash.get('N'))), new Edge(pointHash.get('D'), ToolKit.computeEdges(pointHash.get('D'), pointHash.get('N'))), new Edge(pointHash.get('B'), ToolKit.computeEdges(pointHash.get('B'), pointHash.get('N')))});
        pointHash.get('O').setAdjacencies(new Edge[]{new Edge(pointHash.get('M'), ToolKit.computeEdges(pointHash.get('O'), pointHash.get('M'))), new Edge(pointHash.get('P'), ToolKit.computeEdges(pointHash.get('O'), pointHash.get('P')))});
        pointHash.get('P').setAdjacencies(new Edge[]{new Edge(pointHash.get('O'), ToolKit.computeEdges(pointHash.get('P'), pointHash.get('O'))), new Edge(pointHash.get('B'), ToolKit.computeEdges(pointHash.get('P'), pointHash.get('B')))});
    }

    private static void removeAllDistances(HashMap<Character, Location> pointHash) {

        // Sets all distances back to infinity, to prepare for calculation
        for (Location point : pointHash.values())
            point.removeDistances();
    }

    private static HashMap<Character, Location> createPoints() {

        SampleNodeSet nodes = new SampleNodeSet();
        Iterator<Location> pointIterator = nodes.iterator();
        HashMap<Character, Location> hash = new HashMap<Character, Location>();

        // Starting each node at 'A'
        int asciiPOIs = 65;

        while (pointIterator.hasNext())
            hash.put(new Character((char) (asciiPOIs++)), pointIterator.next());

        return hash;
    }
}