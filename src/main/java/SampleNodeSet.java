
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Meg on 7/22/18.
 */

public class SampleNodeSet implements Iterable<Location> {

    private ArrayList<Location> pointList = new ArrayList<Location>();

    SampleNodeSet() {
        // Locations of the pickups
        pointList.add(new Location(3.0, 3.0, "A"));
        pointList.add(new Location(18.0, 11.0, "B"));
        pointList.add(new Location(4.0, 8.0, "C"));
        pointList.add(new Location(14.0, 7.0, "D"));

        // Where Paths Meet, where the paths intersect
        pointList.add(new Location(4.0, 6.0, "E"));
        pointList.add(new Location(9.0, 5.0, "F"));
        pointList.add(new Location(9.0, 9.0, "G"));
        pointList.add(new Location(5.0, 11.0, "H"));
        pointList.add(new Location(9.0, 12.0, "I"));
        pointList.add(new Location(13.0, 13.0, "J"));
        pointList.add(new Location(15.0, 11.0, "K"));
        pointList.add(new Location(13.0, 9.0, "L"));
        pointList.add(new Location(14.0, 5.0, "M"));
        pointList.add(new Location(16.0, 9.0, "N"));
        pointList.add(new Location(17.0, 4.0, "O"));
        pointList.add(new Location(19.0, 8.0, "P"));
    }

    public Iterator<Location> iterator() {
        return pointList.iterator();
    }
}
